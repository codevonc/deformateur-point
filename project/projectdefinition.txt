// Supported platforms - can be [Win64;OSX]
Platform=Win64;OSX

// Type of project - can be [Lib;DLL;App]
Type=DLL

// API dependencies
APIS=cinema.framework;mesh_misc.framework;math.framework;crypt.framework;python.framework;core.framework;

// C4D component
C4D=true

stylecheck=false // must be set after c4d=true

// Custom ID
ModuleId=1041575
