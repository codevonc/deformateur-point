#include <vector>
#include <map>
#include <string>
#include <algorithm>
#include "c4d.h"
#include "c4d_symbols.h"
#include "c4d_falloffdata.h"
#include "main.h"
#include "Ovoncdefpoint.h"
using namespace std;

#define MODULE_ID 1041575


class VoncDefPoint : public ObjectData
{
private:
	const String VERSION = "2.0";

	Int32 mode = VONC_DEFPOINT_MODE_PER;
	Float intensite = 1.;
	Vector position = Vector();
	Vector echelle = Vector(1.);
	Vector rotation = Vector();

	static Vector MixVec(Vector v1, Vector v2, Float t);

	virtual void Recup(BaseObject* op);


protected:
	AutoAlloc<C4D_Falloff> falloff;
	Int32 falloff_dirty;

	Bool InitFalloff(GeListNode* node_) {
		if (!this->falloff) return false;
		auto* op = static_cast<BaseObject*>(node_);
		if (!op) return false;
		auto* bc = op->GetDataInstance();
		if (!bc) return false;
		return this->falloff->InitFalloff(bc, op->GetDocument(), op);
	}

	Int32 GetFalloffDirty(GeListNode* node_, BaseDocument* doc) {
		if (!this->falloff) return false;
		auto* op = static_cast<BaseObject*>(node_);
		if (!op) return false;
		auto* bc = op->GetDataInstance();
		if (!bc) return false;
		return this->falloff->GetDirty(doc, bc);
	}

public:
	virtual Bool Init				(GeListNode* node);
	virtual Bool Message			(GeListNode* node, Int32 type, void* data);
	virtual Bool GetDEnabling		(GeListNode* node, const DescID &id, const GeData &t_data, DESCFLAGS_ENABLE flags, const BaseContainer* itemdesc);
	virtual Bool ModifyObject		(BaseObject* op, BaseDocument* doc, BaseObject* mod, const Matrix& op_mg, const Matrix& mod_mg, Float lod, Int32 flags, BaseThread* thread);

	static NodeData* Alloc(void) { return NewObjClear(VoncDefPoint); }


	virtual DRAWRESULT Draw(BaseObject* op, DRAWPASS pass, BaseDraw* bd, BaseDrawHelp* bh) override {
		if (!this->InitFalloff(op)) return DRAWRESULT::FAILURE;
		BaseContainer* bc = op->GetDataInstance();
		if (!this->falloff->Draw(bd, bh, pass, bc)) return DRAWRESULT::FAILURE;
		return ObjectData::Draw(op, pass, bd, bh);
	}

	virtual Int32 GetHandleCount(BaseObject* op) override {
		if (!this->InitFalloff(op)) return 0;
		BaseContainer* bc = op->GetDataInstance();
		Int32 count = this->falloff->GetHandleCount(bc);
		return count;
	}

	virtual void GetHandle(BaseObject* op, Int32 index, HandleInfo& info) override {
		if (!this->InitFalloff(op)) return;
		BaseContainer* bc = op->GetDataInstance();
		this->falloff->GetHandle(index, bc, info);
	}

	virtual void SetHandle(BaseObject* op, Int32 index, Vector pos, HandleInfo const& info) override {
		if (!this->InitFalloff(op)) return;
		BaseContainer* bc = op->GetDataInstance();
		this->falloff->SetHandle(index, pos, bc, info);
	}

	virtual void CheckDirty(BaseObject* op, BaseDocument* doc) override {
		ObjectData::CheckDirty(op, doc);
		Int32 dirty_count = this->GetFalloffDirty(op, doc);
		if (dirty_count != this->falloff_dirty) {
			this->falloff_dirty = dirty_count;
			op->SetDirty(DIRTYFLAGS::DATA);
		}
	}

	virtual Bool GetDDescription(GeListNode* node_, Description* desc, DESCFLAGS_DESC& flags) override {
		if (!ObjectData::Init(node_)) return false;
		if (!desc || !desc->LoadDescription(node_->GetType())) return false;
		flags |= DESCFLAGS_DESC::LOADED;
		if (!this->falloff->AddFalloffToDescription(desc, nullptr, flags)) return false;
		return ObjectData::GetDDescription(node_, desc, flags);
	}

};

Vector VoncDefPoint::MixVec(Vector v1, Vector v2, Float t) {
	return v1 + (v2 - v1) * t;
}

Bool VoncDefPoint::Init(GeListNode* node) {

	if (!ObjectData::Init(node)) return false;
	if (!this->InitFalloff(node)) return false;

	BaseObject*		 op = (BaseObject*)node;
	BaseContainer* donnees = op->GetDataInstance();

	donnees->SetFloat(VONC_DEFPOINT_INTENSITE, intensite);
	donnees->SetVector(VONC_DEFPOINT_POSITION, position);
	donnees->SetVector(VONC_DEFPOINT_ECHELLE, echelle);
	donnees->SetVector(VONC_DEFPOINT_ROTATION, rotation);
	donnees->SetInt32(VONC_DEFPOINT_MODE, mode);

	donnees->SetString(VONC_DEFPOINT_INFO, "v " + VERSION + " - code.vonc.fr");

	return true;
}

void VoncDefPoint::Recup(BaseObject* op) {

	BaseContainer* donnees = op->GetDataInstance();

	intensite = donnees->GetFloat(VONC_DEFPOINT_INTENSITE);
	position = donnees->GetVector(VONC_DEFPOINT_POSITION);
	echelle = donnees->GetVector(VONC_DEFPOINT_ECHELLE);
	rotation = donnees->GetVector(VONC_DEFPOINT_ROTATION);
	mode = donnees->GetInt32(VONC_DEFPOINT_MODE);

	donnees->SetString(VONC_DEFPOINT_INFO, "v " + VERSION + " - code.vonc.fr");
}

Bool VoncDefPoint::ModifyObject(BaseObject* mod, BaseDocument* doc, BaseObject* op, const Matrix& op_mg, const Matrix& mod_mg, Float lod, Int32 flags, BaseThread* thread)
{

	// Si pas de points, terminer

	if (!op->IsInstanceOf(Opoint)) return true;

	Int32 nbPoints = ((PointObject*)op)->GetPointCount();
	if (nbPoints == 0) return true;


	// R�cup�re les param�tres

	Recup(mod);

	if (intensite < 0.00000000001 && intensite > -0.00000000001) return true;


	Vector* points = ((PointObject*)op)->GetPointW();
	Float32* weight = ((PointObject*)op)->CalcVertexMap(mod);
	Vector pointAbs;
	Vector pointAbsNouveau;
	Vector pointRelMod;
	Float fac;
	Float fo;
	Matrix op_mg_inv = ~op_mg;
	Matrix mod_mg_inv = ~mod_mg;
	Matrix mod_mg_rot = Matrix(mod_mg);
	mod_mg_rot.off = Vector();


	// Matrice de modification

	Matrix matPosition = Matrix();
	matPosition.off = position;

	Matrix matEchelle = Matrix();
	matEchelle.sqmat *= echelle;
	//matEchelle.Scale(echelle);

	Matrix matRotation = HPBToMatrix(rotation, ROTATIONORDER::XYZLOCAL);

	Matrix mat = Matrix();

	switch (mode) {

		case VONC_DEFPOINT_MODE_PER: {
			mat = matPosition * matEchelle * matRotation;
		} break;

		case VONC_DEFPOINT_MODE_PRE: {
			mat = matPosition * matRotation * matEchelle;
		} break;

		case VONC_DEFPOINT_MODE_EPR: {
			mat = matEchelle * matPosition * matRotation;
		} break;

		case VONC_DEFPOINT_MODE_ERP: {
			mat = matEchelle * matRotation * matPosition;
		} break;

		case VONC_DEFPOINT_MODE_RPE: {
			mat = matRotation * matPosition * matEchelle;
		} break;

		case VONC_DEFPOINT_MODE_REP: {
			mat = matRotation * matEchelle * matPosition;
		} break;

		default:
			break;
	}


	// Calcul des nouveaux points

	for (Int32 i = 0; i < nbPoints; i++) {

		pointAbs = op_mg * points[i];

		fac = intensite;
		if (weight) fac *= weight[i];

		falloff->Sample(pointAbs, &fo);
		fac *= fo;

		if (fac < 0.00000000001 && fac > -0.00000000001) continue;

		pointRelMod = mod_mg_inv * pointAbs;

		pointRelMod = mat * pointRelMod;

		pointAbsNouveau = mod_mg * pointRelMod;

		points[i] = op_mg_inv * MixVec(pointAbs, pointAbsNouveau, fac);
	}

	DeleteMem(weight);

	op->Message(MSG_UPDATE);

	return true;
}

//virtual Bool Message(GeListNode* node_, Int32 msg, void* pdata) override {
Bool VoncDefPoint::Message(GeListNode* node, Int32 type, void* data)
{
	if (!this->InitFalloff(node)) return false;
	if (!this->falloff->Message(type, nullptr, data)) return false;

	if (type == MSG_MENUPREPARE) {
		((BaseObject*)node)->SetDeformMode(true);
	}

	return ObjectData::Message(node, type, data);

}

Bool VoncDefPoint::GetDEnabling(GeListNode* node, const DescID &id, const GeData &t_data, DESCFLAGS_ENABLE flags, const BaseContainer* itemdesc) {

	return ObjectData::GetDEnabling(node, id, t_data, flags, itemdesc);
}


Bool EnregistreVoncDefPoint(void)
{
	return RegisterObjectPlugin(MODULE_ID, GeLoadString(VONC_DEFPOINT_NOM), OBJECT_MODIFIER, VoncDefPoint::Alloc, "Ovoncdefpoint"_s, AutoBitmap("Ovoncdefpoint.tif"_s), 0);
}
